# API URLS
POSTAL_CODE_API = "https://datanova.laposte.fr/api/records/1.0/"
CITIES_POPULATION_API = "https://data.opendatasoft.com/api/records/1.0/"
DPT_POPULATION_API = "https://public.opendatasoft.com/api/records/1.0/"

# Department population year
DPT_YEAR = 2018

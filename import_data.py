"""
OSM POSTAL CODE AND POPULATION IMPORT:
- Postal codes of French cities
- Population of French cities and departments

Copyright (C) 2019 High Testing R&D, ERL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
"""

import logging
import requests
import urllib
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import PostalCode, Base, PopulationInsee
from config import CITIES_POPULATION_API, POSTAL_CODE_API, DPT_POPULATION_API

# Logging
logging.basicConfig(filename='log', filemode='w', level=logging.INFO)

# Create engine
engine = create_engine('sqlite:///data.db')

# Create tables
Base.metadata.create_all(engine)

# Create session
DBSession = sessionmaker(bind=engine)
session = DBSession()


def opendatasoft_api(data_set: str, api_url: str, model: Base, response_format: str = 'json',
                     api_type: str = 'download', **facets):
    # url
    api_url = urllib.parse.urljoin(api_url, api_type)
    # API call
    logging.info("#############")
    logging.info("Calling API")
    logging.info("#############")

    params = [
        ('dataset', data_set),
        ('format', response_format),
    ]
    for key, value in facets.items():
        params.append((key, value))

    response = requests.get(api_url, params=params)
    logging.info("Response status: %s", response.status_code)

    # Response status
    if response.status_code != 200:
        raise RuntimeError("API returned an invalid response code: %s" % response.status_code)

    # Writing data
    logging.info("writing data to database")
    for record in response.json():
        try:
            db_row = model(record)
            session.add(db_row)
        except KeyError as err:
            logging.warning("%s - Data: %s", err, record)
    session.commit()
    session.close()


if __name__ == '__main__':

    # Download all post codes
    opendatasoft_api(
        data_set='laposte_hexasmal',
        api_url=POSTAL_CODE_API,
        model=PostalCode,
        response_format='json',
        api_type='download'
    )

    # Download cities population
    opendatasoft_api(
        data_set='population-francaise-communes@public',
        api_url=CITIES_POPULATION_API,
        model=PopulationInsee,
        response_format='json',
        api_type='download',
        **{'refine.filename': '2016'},          # 'filename' = annee de recensement
    )

    # Download departments population
    opendatasoft_api(
        data_set='population-francaise-par-departement-2018',
        api_url=DPT_POPULATION_API,
        model=PopulationInsee,
        response_format='json',
        api_type='download',
    )

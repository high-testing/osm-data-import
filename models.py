"""
OSM POSTAL CODE AND POPULATION IMPORT

Copyright (C) 2019 High Testing R&D, ERL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <https://www.gnu.org/licenses/lgpl-3.0.en.html>.
"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from .import_data import logging
from config import DPT_YEAR


Base = declarative_base()


class PostalCode(Base):
    __tablename__ = 'postal_codes'
    id = Column(Integer, primary_key=True)
    code_commune_insee = Column(String(5))
    nom_de_la_commune = Column(String(50))
    code_postal = Column(String(5))

    def __init__(self, record):
        kwargs = {
            'code_commune_insee': record["fields"]["code_commune_insee"],
            'nom_de_la_commune': record["fields"]["nom_de_la_commune"],
            'code_postal': record["fields"]["code_postal"]
        }
        super(PostalCode, self).__init__(**kwargs)


class PopulationInsee(Base):
    __tablename__ = 'population'
    id = Column(Integer, primary_key=True)
    division = Column(String(15))
    code_division = Column(String(4))
    annee_recensement = Column(String(4))           # API field name is "filename"...
    code_insee = Column(String(5))
    nom_de_la_division = Column(String(50))
    population_municipale = Column(Integer)
    population_comptee_a_part = Column(Integer)
    population_totale = Column(Integer)

    def __init__(self, record):
        division = ''
        code_division = ''
        population_municipale = 0
        population_comptee_a_part = 0
        population_totale = 0
        nom_de_la_division = ''
        annee_recensement = ''
        code_insee = ''

        if 'code_commune' in record["fields"].keys():
            division = 'commune'
            code_division = record["fields"]["code_commune"]
            annee_recensement = record["fields"]["filename"]
            code_insee = record["fields"]["code_insee"]
            nom_de_la_division = record["fields"]["nom_de_la_commune"]
            population_municipale = record["fields"]["population_municipale"]
            population_comptee_a_part = record["fields"]["population_comptee_a_part"]
            population_totale = record["fields"]["population_totale"]

        elif 'code_departement' in record["fields"].keys():
            division = 'departement'
            code_division = record["fields"]["code_departement"]
            annee_recensement = DPT_YEAR
            nom_de_la_division = record["fields"]["departement"]
            population_totale = record["fields"]["population"]

        else:
            logging.warning("Invalid response: can't identify the division type")

        kwargs = {
            'annee_recensement': annee_recensement,
            'code_insee': code_insee,
            'nom_de_la_division': nom_de_la_division,
            'population_municipale': int(population_municipale),
            'population_comptee_a_part': int(population_comptee_a_part),
            'population_totale': int(population_totale),
            'division': division,
            'code_division': code_division
        }
        super(PopulationInsee, self).__init__(**kwargs)
